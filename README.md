Relaylist is a new way to share your relays with the world. Just submit your relay and it will be displayed on top of the startpage.

## Installation

Make sure you have Python 2 (with pip) installed and if you're on Windows - added to your PATH variable. Under Windows and Linux (as root) you run the following commands:

```
git clone https://git.koyu.space/koyu/relaylist
cd relaylist
pip install -r requirements.txt
python app.py
```

Then the server should start and you're ready to go.

## Issues

Please submit issues via the issue tab on this site.