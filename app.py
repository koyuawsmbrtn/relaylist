from bottle import request, app, get, static_file, route, redirect, response, error, run
from os import listdir
from os.path import isfile, join
import os
import time
import validators
import re

def remove_tags(raw_html):
    cleanr =re.compile('<.*?>')
    cleantext = re.sub(cleanr,'', raw_html)
    return cleantext

def sorted_ls(path):
    mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
    return list(sorted(os.listdir(path), key=mtime))

def get_rows(sequence, num):
    count = 1
    rows = list()
    cols = list()
    for item in sequence:
        if count == num:
            cols.append(item)
            rows.append(cols)
            cols = list()
            count = 1
        else:
            cols.append(item)
            count += 1
    if count > 0:
        rows.append(cols)
    return rows

def head():
    h = "<link rel=\"stylesheet\" href=\"/style.css\">"
    h += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
    h += "<nav><div class=\"nav\">"
    x = listdir("data/")
    for dir in x:
        f = open("data/" + dir + "/title.txt", "r")
        title = f.read()
        f.close()
        if not dir == "ab":
            if not dir == "a":
                if not dir == "submit":
                    h += "<a href=\"/" + dir + "/\" style=\"color:#fff;\">" + title + "</a> | "
    h += "<a href=\"/ab/\" style=\"color:#fff;\">About Relaylist</a>"
    h += "</div></p><p><a href=\"/submit/\" class=\"submit\">Submit</a></nav><p><a href=\"/\"><img src=\"/logo.png\"></a></p><hr>"
    return h

@get('/<filename>')
def images(filename):
    return static_file(filename, root='static/')

@route("/")
def all():
    redirect("https://relays.koyu.space/relays/")

@route("/<name>/")
def output(name):
    f = open("data/" + name + "/title.txt", "r")
    title = f.read()
    f.close()
    output = "<title>Relaylist - " + title + "</title>"
    # s = [f for f in listdir("data/" + name + "/") if isfile(join("data/"+ name + "/", f))]
    s = sorted_ls("data/" + name + "/")
    s.remove("title.txt")
    output += head()
    output += "<div class=\"content\">"
    if not name == "ab":
        if not name == "submit":
                output += "<p>This list contains relays for Mastodon 2.5 or above. Don't know what relays are? Don't care if you're a user. It's just a new feature to find other instances more quickly when your instance has been newly established.</p>"
                output += "<p><b style=\"color:red;\">Warning: Don't add too many relays since your database will grow like crazy</b></p>"
                output += "<h1>" + title + "</h1>"
                output += "<ul>"
                for file in s:
                    f = open("data/" + name + "/" + file, "r")
                    a = f.read()
                    f.close()
                    t = file.replace(".txt", "")
                    output += "<li><a href=\"" + a + "\" target=\"_blank\">" + t + "</a></li>"
                output += "</ul>"
        else:
            output += "<form action=\"/send/\" method=\"post\">"
            output += "<input type=\"hidden\" name=\"blubb\" value=\"\">"
            output += "</p>"
            output += "<p>URL: <input type=\"url\" name=\"url\"></p>"
            output += "<p><input type=\"submit\" name=\"submit\" value=\"Submit\"></p>"
            output += "</form>"
    else:
        output += "<p>Relaylist v. 0.3</p>"
        output += "<p>Written by <a href=\"https://web.koyu.space\" target=\"_blank\">koyu</a></p>"
        output += "<p>&copy; Relaylist " + time.strftime("%Y") + "</p>"
    output += "</div>"
    return output

@route("/style.css")
def style():
    response.content_type = "text/css"
    f = open("style.css")
    s = f.read()
    f.close()
    return s

@route("/send/", method="POST")
def send():
    url = request.forms.get("url") # pylint: disable=no-member
    url = remove_tags(url)
    blubb = "relays"
    iserror = False
    if not url == "":
        if not blubb == "" and "/inbox" in url and "http" in url:
            if os.path.exists("data/" + blubb + "/" + url.split("/")[2] + ".txt"):
                alreadyexists = True
            else:
                f = open("data/" + blubb + "/" + url.split("/")[2] + ".txt", "w")
                f.write(url)
                f.close()
                redirect("https://relays.koyu.space/")
        else:
            iserror = True
    else:
        iserror = True
    if iserror:
        output = "Error - Relaylist"
        output += head()
        output += "<h1>Error: Not a valid Relay URL</h1>"
        return output
    if alreadyexists:
        output = "Error - Relaylist"
        output = head()
        output += "<h1>Error: Relay already exists!</h1>"
        return output

@error(404)
def error404(error):
    output = head()
    output += "<img src=\"https://http.cat/404.jpg\" height=\"300\">"
    return output

@error(500)
def error500(error):
    output = head() # pylint: disable=used-before-assignment
    x = listdir("data/")
    for dir in x:
        f = open("data/" + dir + "/title.txt", "r")
        title = f.read()
        f.close()
        if not dir == "ab":
            if not dir == "a":
                if not dir == "submit":
                    head += "<a href=\"/" + dir + "/\" style=\"color:#fff;\">" + title + "</a> | "
    output += "<a href=\"/ab/\" style=\"color:#fff;\">About Relaylist</a>"
    output += "</p><p><a href=\"/submit/\">Submit</a></p><p><a href=\"/\"><img src=\"/logo.png\"></a></p><hr>"
    output = head
    output += "<img src=\"https://http.cat/500.jpg\" height=\"300\">"
    return output

run(host="127.0.0.1", port=8080, server="tornado")
